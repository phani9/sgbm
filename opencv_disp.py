import cv2
import numpy as np
import glob
import os

# disparity range tuning
window_size = 15
min_disp = 0
num_disp = 160 - min_disp

left_matcher = cv2.StereoSGBM_create(
    minDisparity=0,
    numDisparities=160,  # max_disp has to be dividable by 16 f. E. HH 192, 256
    blockSize=5,
    P1=8 * 3 * window_size ** 2,
    # wsize default 3; 5; 7 for SGBM reduced size image; 15 for SGBM full size image (1300px and above); 5 Works nicely
    P2=32 * 3 * window_size ** 2,
    disp12MaxDiff=1,
    uniquenessRatio=10,
    speckleWindowSize=10,
    speckleRange=2,
    preFilterCap=63,
    mode=cv2.STEREO_SGBM_MODE_SGBM_3WAY
)
right_matcher = cv2.ximgproc.createRightMatcher(left_matcher)
# FILTER Parameters
lmbda = 80000
sigma = 1.5
visual_multiplier = 1.0

# Weighted least squares filter to fill sparse (unpopulated) areas of the disparity map
    # by aligning the images edges and propagating disparity values from high- to low-confidence regions
wls_filter = cv2.ximgproc.createDisparityWLSFilter(matcher_left=left_matcher)
wls_filter.setLambda(lmbda)
wls_filter.setSigmaColor(sigma)



fnames = glob.glob('image_2/*')
fnames = np.sort(fnames)
for fname in fnames:
    lname = fname
    rname = fname.replace('image_2','image_3')
    if os.path.exists(rname)<1:
        continue

    imgL = cv2.imread(lname,0)
    imgL = cv2.resize(imgL, (672, 376))
    imgR = cv2.imread(rname,0)
    imgR = cv2.resize(imgR, (672, 376))
    #imgL = cv2.resize(imgL, (0, 0), fx=0.5, fy=0.5)
    #imgR = cv2.resize(imgR, (0, 0), fx=0.5, fy=0.5)
    rows, cols = imgL.shape[:2]

    a = np.zeros((rows, 320), dtype = np.uint8)

    imgL = np.hstack((a, imgL))
    imgR = np.hstack((a, imgR))
    # Get depth information/disparity map using SGBM
    displ = left_matcher.compute(imgL, imgR).astype(np.float32)/16
    outl = np.clip(displ, 0, 160)
    outl = outl.astype(np.uint8)
    outl = outl[:,320:]


    dispr = right_matcher.compute(imgR, imgL).astype(np.float32)/16
    displ = np.int16(displ)
    dispr = np.int16(dispr)



    filteredImg = wls_filter.filter(displ, imgL, None, dispr)  # important to put "imgL" here!!!
    #filteredImg = cv2.normalize(src=filteredImg, dst=filteredImg, beta=0, alpha=255, norm_type=cv2.NORM_MINMAX);
    filteredImg = filteredImg + 16
    filteredImg[filteredImg<0] = 0
    filteredImg[filteredImg>255] = 255
    filteredImg = np.uint8(filteredImg)
    filteredImg = filteredImg[:,320:]



    #filteredImg = cv2.bilateralFilter(filteredImg,11, 125,125)
    imgL =imgL[:,320:]
    print (imgL.shape, filteredImg.shape)
    filteredImg = cv2.ximgproc.guidedFilter(imgL,filteredImg,8,500)
    oname = fname.replace('image_2','disp_opencv')
    out = np.vstack((outl, filteredImg))

    out= np.dstack((out, out, out))
    im1 = cv2.imread(lname)
    im1 = cv2.resize(im1, (672, 376))
    im2 = cv2.imread(rname)
    im2 = cv2.resize(im2, (672, 376))

    im = np.vstack((im1, im2))
    out = np.hstack((im, out))
    cv2.imwrite(oname, out)
    print (oname)
    continue

    cv2.imshow('c', out)
    cv2.waitKey(50)
    continue

    disparity = stereo.compute(imgL, imgR).astype(np.float32) / 16.0
    disparity[disparity<0] = 0
    disparity = disparity[:,512:]*2
    disparity[disparity>192] = 192
    disparity = disparity.astype(np.uint8)
    disparity = cv2.medianBlur(disparity, 3)



    mask = disparity<10
    mask = mask.astype(np.uint8)*255
    #disparity = cv2.inpaint(disparity,mask,3,cv2.INPAINT_TELEA)
    oname = 'disp_opencv/' + fname.split('/')[-1]
    cv2.imwrite(oname, disparity)
    print (oname)
#plt.imshow(disparity, 'gray')
#plt.show()
